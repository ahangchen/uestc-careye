#include <rc_main/main.h>
#include <rc_main/rc_param.h>
#include <rc_log/slog.hpp>
#include <vpu/vpu_tools_common.hpp>


bool ParseAndCheckCommandLine(int argc, char *argv[]) {
    // ---------------------------Parsing and validation of input args--------------------------------------
    slog::info << "Parsing input parameters" << slog::endl;

    gflags::ParseCommandLineNonHelpFlags(&argc, &argv, true);
    if (FLAGS_h) {
        showUsage();
        return false;
    }

    if (FLAGS_c.empty()) {
        slog::warn << "No config file input , Use default file" << slog::endl;
    }

    if (FLAGS_generate) {
        slog::warn << "Generate default config file" << slog::endl;
        generateDefaultConfig();
        return false;
    }

    return true;
}
