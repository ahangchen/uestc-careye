//
// Created by Pulsar on 2020/5/16.
//
#include <rc_system/config.h>

std::atomic<bool> flag = true;

void sig_handler(int signum) {
    if (signum == SIGINT) {
        if (flag) {
            std::cout << "Exiting..." << std::endl;
            flag = false;
        } else {
            exit(0);
        }
    }
}

int main(int argc, char **argv) {
    std::shared_ptr<rccore::common::Config> config = std::make_shared<rccore::common::Config>("config.yml");
    config->Load();
    while (flag) {}
    return 1;
}