//
// Created by Pulsar on 2021/8/26.
//

#include <libhumandetect/humandetector.h>
#include <rc_system/functions.h>
#include <vector>
#include <rc_log/slog.hpp>


void *InitCallback() {
    auto *cascada = new cv::CascadeClassifier("haarcascades/haarcascade_frontalface_default.xml");
    return cascada;
}

void *BeforeDetcetCallback(void *args) {
    return args;
}

void DetectCallback(cv::Mat &frame, void *args, const std::shared_ptr<rccore::message::MoveMessage> &p_move_message) {
    auto *cascada = (cv::CascadeClassifier *) args;
    WHEEL_DATA wheelData = {0.0, 0.0, 0.0, 0.0};
    p_move_message->push_message(wheelData);
    if (not cascada->empty()) {
        std::vector<cv::Rect> bodies;
        cv::Mat frame_gray;
        cvtColor(frame, frame_gray, cv::COLOR_BGR2GRAY);//转灰度化，减少运算
        cascada->detectMultiScale(frame_gray, bodies, 1.1, 4, cv::CASCADE_DO_ROUGH_SEARCH, cv::Size(70, 70),
                                  cv::Size(1000, 1000));
//        printf("检测到人脸个数：%d\n", bodies.size());
        for (int i = 0; i < bodies.size(); i++) {
            cv::rectangle(frame, bodies[i], cv::Scalar(255, 0, 0), 2, 8, 0);
        }
    }
//    slog::info << frame.size() << slog::endl;
    cv::imshow("frame", frame);
    cv::waitKey(1);
}

void *AfterDetcetCallback(void *args) {
    auto *cascada = (cv::CascadeClassifier *) args;
}

void OnThreadBreakCallback(void *args) {
    auto *cascada = (cv::CascadeClassifier *) args;
}