project(RobotCenter)

message(STATUS ===================================RobotCenter============================)

set(ENV_CMAKE_FILES_PATH ${PROJECT_SOURCE_DIR}/../cmake/)

include(${ENV_CMAKE_FILES_PATH}/rcMpiCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcBoostCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcPlathformConfigCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcGlobalIncludeCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcOpenGLCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcOpenCVCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcPistacheCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcLibrealsense2Cmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcProtobufCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcLibservCmake.cmake)


include(${PROJECT_SOURCE_DIR}/src/cmake/Qt5Import.cmake)

include_directories(${PROJECT_SOURCE_DIR}/../robot_client/src/include)


include(${PROJECT_SOURCE_DIR}/src/cmake/RC_ServerCenterCmake.cmake)
include(${PROJECT_SOURCE_DIR}/src/cmake/RC_MainAPPCmake.cmake)

include(${PROJECT_SOURCE_DIR}/test/CMakeLists.txt)
message(STATUS ===================================DONE============================)
