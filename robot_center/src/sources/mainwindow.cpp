#include <mainwindow.h>
#include <iostream>
#include <asio/rc_socket_server.h>
#include <asio/rc_tcp_server_boost.h>
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this);

}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_start_server_clicked() {
    if (ui->edit_IP->isEnabled() and ui->edit_Port->isEnabled()) {
        ui->edit_IP->setEnabled(false);
        ui->edit_Port->setEnabled(false);
        ui->pushButton_start_server->setText("关闭服务");
//        try {
//            std::cout << "server start." << std::endl;
//            boost::asio::io_service ios;
//            rc_tcp_server_boost rcTcpServerBoost(1090, ios);
//            ios.run();
//        }
//        catch (std::exception &_e) {
//            ui->edit_IP->setEnabled(true);
//            ui->edit_Port->setEnabled(true);
//            ui->pushButton_start_server->setText("开启服务");
//            std::cout << _e.what() << std::endl;
//        }
    } else {
        ui->edit_IP->setEnabled(true);
        ui->edit_Port->setEnabled(true);
        ui->pushButton_start_server->setText("开启服务");
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
        case Qt::Key_W:
            ui->pushButton_W->setDown(true);
            break;
        case Qt::Key_S:
            ui->pushButton_S->setDown(true);
            break;
        case Qt::Key_A:
            ui->pushButton_A->setDown(true);
            break;
        case Qt::Key_D:
            ui->pushButton_D->setDown(true);
            break;
        case Qt::Key_R:
            ui->pushButton_R->setDown(true);
            break;
        case Qt::Key_E:
            ui->pushButton_E->setDown(true);
            break;
        case Qt::Key_Q:
            ui->pushButton_Q->setDown(true);
            break;
        case Qt::Key_Z:
            ui->pushButton_up->setDown(true);
            break;
        case Qt::Key_C:
            ui->pushButton_down->setDown(true);
            break;
        default:
            break;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event) {
    switch (event->key()) {
        case Qt::Key_W:
            ui->pushButton_W->setDown(false);
            ui->pushButton_W->clicked();
            break;
        case Qt::Key_S:
            ui->pushButton_S->setDown(false);
            ui->pushButton_S->clicked();
            break;
        case Qt::Key_A:
            ui->pushButton_A->setDown(false);
            ui->pushButton_A->clicked();
            break;
        case Qt::Key_D:
            ui->pushButton_D->setDown(false);
            ui->pushButton_D->clicked();
            break;
        case Qt::Key_R:
            ui->pushButton_R->setDown(false);
            ui->pushButton_R->clicked();
            break;
        case Qt::Key_E:
            ui->pushButton_E->setDown(false);
            ui->pushButton_E->clicked();
            break;
        case Qt::Key_Q:
            ui->pushButton_Q->setDown(false);
            ui->pushButton_Q->clicked();
            break;
        case Qt::Key_Z:
            ui->pushButton_up->setDown(false);
            ui->pushButton_up->clicked();
            break;
        case Qt::Key_C:
            ui->pushButton_down->setDown(false);
            ui->pushButton_down->clicked();
            break;
        default:
            break;

    }
}

void MainWindow::on_pushButton_Q_clicked() {
    std::cout << "Q pressed" << std::endl;
}

void MainWindow::on_pushButton_W_clicked() {
    std::cout << "W pressed" << std::endl;
}

void MainWindow::on_pushButton_A_clicked() {
    std::cout << "A pressed" << std::endl;
}

void MainWindow::on_pushButton_S_clicked() {
    std::cout << "S pressed" << std::endl;
}

void MainWindow::on_pushButton_D_clicked() {
    std::cout << "D pressed" << std::endl;
}

void MainWindow::on_pushButton_R_clicked() {
    std::cout << "R pressed" << std::endl;
}

void MainWindow::on_pushButton_E_clicked() {
    std::cout << "E pressed" << std::endl;
}

void MainWindow::on_clicked() {

    std::cout << " on_clicked" << std::endl;
}
