#include <mainwindow.h>
#include <QApplication>

#include <QtQml/QQmlApplicationEngine>
#include <QtGui/QFont>
#include <QtGui/QFontDatabase>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow window;
    window.show();
//    QFontDatabase::addApplicationFont(":/fonts/DejaVuSans.ttf");
//    app.setFont(QFont("DejaVu Sans"));
//
//    QQmlApplicationEngine engine(QUrl("qrc:/qml/dashboard.qml"));
//    if (engine.rootObjects().isEmpty())
//        return -1;

    return app.exec();
}
