//
// Created by Pulsar on 2021/8/23.
//

#include <rc_task/rcBaseTask.h>
#include <rc_task/rcTaskVariable.h>
#include <rc_log/slog.hpp>

namespace rccore {
    namespace task {
        void BaseTask::Stop() {
            m_isStop = true;
        }

        void BaseTask::Pause() {
            std::unique_lock<std::mutex> m_locker(m_mutex);
            m_isPause = true;
            m_cv.notify_one();
        }

        void BaseTask::Resume() {
            std::unique_lock<std::mutex> m_locker(m_mutex);
            m_isPause = false;
            m_cv.notify_one();
        }

        void BaseTask::Join() {
            if (m_thread)
                m_thread->join();
        }

        void BaseTask::Detach() {
            if (m_thread)
                m_thread->detach();
        }

        BaseTask::BaseTask(std::shared_ptr<common::Context> _pcontext) :
                pcontext(std::move(_pcontext)) {
            if (not task::task_variable::CONTEXT_IS_INITED) {
                slog::err << "全局上下文未被初始化!" << slog::endl;
                exit(-1);
            }
        }

        BaseTask::~BaseTask() {
        }
    }
}