//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcWebStream.h>
#include <iostream>
#include <rc_log/slog.hpp>
#include <utility>
#include <rc_task/rcTaskVariable.h>

namespace rccore {
    namespace task {
        void WebStreamTask::Run() {
            slog::info << "WEB任务启动中" << slog::endl;
            int thr = Pistache::hardware_concurrency();
            slog::info << "CPU核心数 = " << thr << slog::endl;
            slog::info << "HTTP服务线程 " << thr << " 个" << slog::endl;

            m_thread = new std::thread([=]() {
                try {
                    task::task_variable::TASK_NUM += 1;
                    prcHttpdRest.reset(new rccore::network::httpd::RcHttpdRest(pcontext));
                    prcHttpdRest->init(1);
                    prcHttpdRest->start();
                    slog::warn << "HTTP服务终止 " << slog::endl;
                    task::task_variable::TASK_NUM -= 1;
                } catch (std::runtime_error &e) {
                    task::task_variable::TASK_NUM -= 1;
                    slog::err << "HTTP启动失败 " << slog::endl;
                    slog::err << e.what() << slog::endl;
                }
            });
        }

        WebStreamTask::WebStreamTask(std::shared_ptr<common::Context> pcontext) : BaseTask(std::move(pcontext)) {

        }

        void WebStreamTask::Stop() {
            m_isStop = true;
            slog::warn << "HTTP 停止中..." << slog::endl;
            prcHttpdRest->stop();
        }
    }
}

