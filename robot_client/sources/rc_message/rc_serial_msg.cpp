//
// Created by PulsarV on 18-10-30.
//

#include <rc_message/rc_serial_msg.h>

namespace rccore {
    namespace message {
        SerialMessage::SerialMessage(int max_queue_size)
                : BaseMessage<std::map<int, rccore::data_struct::rc_SerialPackage>>
                          (max_queue_size) {

        }

        void SerialMessage::init(std::string device, int freq) {

        }

        int SerialMessage::send(int head, int size, char *message) {

            return 0;
        }

        int SerialMessage::run() {
            return 0;
        }

        int SerialMessage::recive() {
            return 0;

        }
    }
}