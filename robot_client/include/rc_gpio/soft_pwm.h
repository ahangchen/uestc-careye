
#ifndef ROBOCAR_RC_SOFT_PWM_GPIO_H
namespace RC {
    namespace GPIO {
        void pwm_begin(unsigned int gpio);

        void pwm_set_duty_cycle(unsigned int gpio, int dutycycle);

        void pwm_set_frequency(unsigned int gpio, int freq);

        void pwm_start(unsigned int gpio);

        void pwm_stop(unsigned int gpio);

        void pwm_free(unsigned int gpio);
    }
}
#endif//ROBOCAR_RC_SOFT_PWM_GPIO_H